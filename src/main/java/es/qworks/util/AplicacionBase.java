/*
 * Version 1.2
 * 
 * Version 1.2
 * A�adido m�todo leerImagen y sistema de logs
 * 
 * Version 1.1
 * 
 * 
 * 
 */package es.qworks.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

/**
 * @author jmgarcia
 * @version 1.2
 */
public class AplicacionBase {

	protected static BufferedReader br = new BufferedReader(
			new InputStreamReader(System.in));

	// Lee una opcion del teclado
	protected static Integer leerInteger(String prompt) {
		Integer ret;
		try {
			String str;
			System.out.print(prompt);
			str = br.readLine();
			ret = Integer.parseInt(str);
		} catch (IOException e) {
			ret = null;
		} catch (NumberFormatException e) {
			ret = null;
		}
		return ret;
	}

	// Lee una opcion del teclado
	protected static String leerString(String prompt) {
		String ret = null;
		try {
			System.out.print(prompt);
			ret = br.readLine();
		} catch (IOException e) {
			ret = null;
		}
		return ret;
	}

	// Lee una opcion del teclado
	protected static Double leerDouble(String prompt) {
		Double ret = null;
		try {
			String str;
			System.out.print(prompt);
			str = br.readLine();
			ret = Double.parseDouble(str);
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	/**
	 * Lectura de una imagen a partir del nombre del fichero.
	 * 
	 * @param filename
	 * @return
	 */
	protected static BufferedImage leerImagen(String filename) {

		BufferedImage img = null;

		// Encapsula en un objeto File
		try {
			File f = new File(filename);

			if (!f.canRead()) {
				System.err.println("ERROR: La imagen " + filename
						+ " no se ha podido leer correctamente.");
				System.err
						.println("Compruebe la ubicaci�n y permisos del fichero.");
			} else if ((img = ImageIO.read(f)) == null) {
				System.err.println("ERROR: La imagen " + filename
						+ " no se ha podido leer correctamente.");
				System.err
						.println("Compruebe el formato del fichero (Por ejemplo, con otro programa) y verifique");
				System.err
						.println("que satisface los requisitos de esta aplicaci�n.");
			}
		} catch (IOException e) {
			System.err.println("ERROR: La imagen " + filename
					+ " no se ha podido leer correctamente.");
			System.err
					.println("Ocurri� una excepci�n de E/S al leer la imagen desde el disco:");
			System.err.println(e.getMessage());
			e.printStackTrace(System.err);
			return null;
		}

		return img;
	}

	/**
	 * Imprime un mensaje por pantalla
	 * 
	 * @param o
	 */
	protected void print(String msg) {
		System.out.print(msg);
	}

	/**
	 * Imprime un mensaje por pantalla terminando en fin de linea
	 * 
	 * @param o
	 */
	protected void println(String msg) {
		System.out.println(msg);
	}
}
