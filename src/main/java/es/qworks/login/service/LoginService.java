package es.qworks.login.service;

import java.io.Serializable;

import javax.inject.Named;

import es.qworks.login.domain.User;


/**
 * Servicio de login de usuario
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2015
 */
@Named
public interface LoginService extends Serializable {
	
	/**
	 * @return the currentUser
	 */
	public abstract User<?> getCurrentUser();

	/**
	 * @param currentUser the currentUser to set
	 */
	public abstract void setCurrentUser(User<?> currentUser);

	/**
	 * M�todo que valida el usuario y la contrase�a para acceder al sistema
	 * Adem�s carga el perfil y los datos del usuario
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	public abstract boolean authenticate(String username, String password);

}