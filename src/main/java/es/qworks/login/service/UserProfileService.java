/*
 * Version 1.0 6/12/2015
 * 
 *
 * 2013 � Jos� M. Garc�a Dom�nguez
 *
 */
package es.qworks.login.service;

import es.qworks.login.domain.User;

/**
 * @author jmgarcia
 * @version 1.0
 */
public interface UserProfileService {
	
	public User<?> getUserByLogin(String login);
}
