package es.qworks.login.service;

import java.io.Serializable;

import javax.security.auth.login.LoginException;

/**
 * Servicio de autenticaci�n de usuarios
 * 
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2014
 */
public interface AuthenticationService extends Serializable {
	
	public final String PROPERTIES_FILE = "auth_service.props";

	/**
	 * Inicializa el servicio. 
	 * Lee el fichero de propiedades 
	 */
	public abstract void init()  throws Exception;

	/**
	 * Autentica un nombre de usuario y contrase�a
	 * @param username
	 * @param password
	 * @return
	 * @throws LoginException
	 */
	public abstract boolean authenticate(String username, String password)
			throws LoginException;

}