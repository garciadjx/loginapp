package es.qworks.login.service.impl;

import java.io.*;
import javax.security.auth.callback.*;

/**
 * 
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2014
 */
public class MyCallbackHandler implements CallbackHandler {

	private String name;
	private String password;
	
	/**
	 * Constructor 
	 * @param name
	 * @param password
	 */
	public MyCallbackHandler() {
	}
	
	/**
	 * Constructor 
	 * @param name
	 * @param password
	 */
	public MyCallbackHandler(String name, String password) {
		setCredentials(name, password);
	}
	
	
	/**
	 * @param name
	 * @param password
	 */
	public void setCredentials(String name, String password) {
		this.name = name;
		this.password = password;
	}
	

	/**
	 * @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[])
	 */
	@Override
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		for (Callback cb : callbacks) {
			if (cb instanceof NameCallback) {
				((NameCallback) cb).setName(name);
			} else if (cb instanceof PasswordCallback) {
				((PasswordCallback) cb).setPassword(password.toCharArray());
			} else if (cb instanceof TextOutputCallback) {
				System.out.println(((TextOutputCallback) cb).getMessage());
			}
		}
	}

}
