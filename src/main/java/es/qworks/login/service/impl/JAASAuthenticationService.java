/*
 * 
 */
package es.qworks.login.service.impl;

import javax.enterprise.inject.Alternative;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import es.qworks.login.service.AuthenticationService;

/**
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2014
 *
 */
@Alternative
public class JAASAuthenticationService implements AuthenticationService {

	private static final long serialVersionUID = -6693994306703130485L;
	private static String LNAME = "Simple";
	private MyCallbackHandler cbh;
	
	/**
	 * Constructor 
	 */
	public JAASAuthenticationService() {
		
	}

	/**
	 * @see net.povisa.workflows.service.auth.AuthenticationService#init()
	 */
	@Override
	public void init() {
		cbh = new MyCallbackHandler();		
	}

	/**
	 * @see net.povisa.workflows.service.auth.AuthenticationService#authenticate(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean authenticate(String username, String password)
			throws LoginException {
		
		cbh.setCredentials(username, password);
		LoginContext lc = new LoginContext(LNAME, cbh);
		lc.login();
		
		return true;
	}

}
