/*
 * 
 */
package es.qworks.login.service.impl;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.qworks.login.domain.CurrentUser;
import es.qworks.login.domain.User;
import es.qworks.login.service.AuthenticationService;
import es.qworks.login.service.LoginService;
import es.qworks.login.service.UserProfileService;

/**
 * 
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2014
 */
public class LoginServiceImpl implements LoginService {

	private static final long serialVersionUID = 4214732730866970170L;
	private static Logger LOG = LoggerFactory.getLogger(LoginServiceImpl.class);
	
	@Inject
	@DBAuth
	private AuthenticationService authenticationService;
	@Inject 
	private UserProfileService userProfileService;
	
	private User<?> currentUser = null;
	
	/**
	 * Constructor 
	 */
	public LoginServiceImpl() {
		
	}

	/**
	 * @see org.LoginService.custom.povisa.security.service.LoginService#getCurrentUser()
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Produces
	@Named
	@CurrentUser
	@Override
	public User getCurrentUser() {
		if (LOG.isDebugEnabled()) {
			String txt = currentUser!=null?(String)currentUser.getId():"null";
			LOG.debug(" getCurrentUser(): {}", txt);
		}
		return currentUser;
	}

	/**
	 * @see org.LoginService.custom.povisa.security.service.LoginService#setCurrentUser(org.activiti.engine.identity.User)
	 */
	@Override
	public void setCurrentUser(@SuppressWarnings("rawtypes") User currentUser) {
		this.currentUser = currentUser;
	}

	/**
	 * @see org.LoginService.custom.povisa.security.service.LoginService#authenticate(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean authenticate(String username, String password)  {
		User<?> user = null;
		boolean ret = false;
		
		try {
			if (LOG.isDebugEnabled()) LOG.info("Authenticating user " + username);
			ret = authenticationService.authenticate(username, password);
			// Lee el perfil del usuario
		} catch (LoginException e) {
			if (LOG.isDebugEnabled()) LOG.warn("Could not authenticate user " + username);
			return false;
		}
		
		if ((user = userProfileService.getUserByLogin(username)) != null) {
			setCurrentUser(user);
			ret = true;
			LOG.info("AUTHENTICATED USER: " + user.getId());			
		}
		else {
			ret = false;
			LOG.warn("Could not read user profile");
		}
		
		return ret;
	}
	

}
