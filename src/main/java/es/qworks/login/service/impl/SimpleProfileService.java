/**
 * 
 */
package es.qworks.login.service.impl;

import es.qworks.login.domain.SimpleUser;
import es.qworks.login.domain.User;
import es.qworks.login.service.UserProfileService;

/**
 * @author jmgarcia
 *
 */
public class SimpleProfileService implements UserProfileService {

	/**
	 * 
	 */
	public SimpleProfileService() {
		
	}

	/**
	 * @see es.qworks.login.service.UserProfileService#getUserByLogin(java.lang.String)
	 */
	@Override
	public User<?> getUserByLogin(String login) {
		SimpleUser user = new SimpleUser();
		user.setLogin(login);
		user.setFirstName(login);
		return null;
	}

}
