package es.qworks.login.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
 
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.security.auth.login.AccountException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.qworks.login.service.AuthenticationService;
 

/**
 * 
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2014
 */
@Named
@Default
@SessionScoped
public class ActiveDirectoryAuthenticationService implements AuthenticationService {
 
	public static String propsFile = PROPERTIES_FILE;
	
	private static final long serialVersionUID = -6727428967148800926L;
	private static final String DOMAIN_PROP = "domain";
	private static final String CONTEXT_FACTORY_CLASS = "com.sun.jndi.ldap.LdapCtxFactory";
 
	private static Logger LOG = LoggerFactory.getLogger(ActiveDirectoryAuthenticationService.class);
	
	private String ldapServerUrls[];
    private int lastLdapUrlIndex;
    private String domainName;
    
 
    /**
     * Constructor 
     */
    public ActiveDirectoryAuthenticationService() {
        
    }
    
    
    /**
	 * @see net.povisa.workflows.service.auth.AuthenticationService#init()
	 */
    @PostConstruct
    @Override
	public void init() {
    	String domainName = null;
    	InputStream is = null;
    	Properties config = new Properties();
    	
    	try {
    		if ((is = getClass().getClassLoader().getResourceAsStream(propsFile)) != null) {
    			config.load(is);
    			if ((domainName = config.getProperty(DOMAIN_PROP)) != null) {
    				if (LOG.isDebugEnabled()) LOG.debug("Read property {}: {}", DOMAIN_PROP, domainName);
    				this.domainName = domainName.toUpperCase();	
    	    	}
    	    	else {
    	    		LOG.warn("Could not read property {}", DOMAIN_PROP);
    	    	}
    		}
    		else {
    			LOG.warn("Could not open properties file {}", propsFile);
    		}
		} catch (IOException e) {
			LOG.error("Error reading file: {}", propsFile);
			LOG.error(e.getMessage());
			e.printStackTrace();
		}
		
    	// 
		if ((this.domainName == null) && ((domainName = getLocalDomain()) != null)) {
			LOG.warn("Infered domain name from network: {}", domainName);
			this.domainName = domainName.toUpperCase();	
		}
    			
    	
        try {
            ldapServerUrls = nsLookup(domainName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        lastLdapUrlIndex = 0;
    }
 
    
    /**
	 * @see net.povisa.workflows.service.auth.AuthenticationService#authenticate(java.lang.String, java.lang.String)
	 */
    @Override
	public boolean authenticate(String username, String password) throws LoginException {
        if (ldapServerUrls == null || ldapServerUrls.length == 0) {
            throw new AccountException("Unable to find ldap servers");
        }
        if (username == null || password == null || username.trim().length() == 0 || password.trim().length() == 0) {
            throw new FailedLoginException("Username or password is empty");
        }
        int retryCount = 0;
        int currentLdapUrlIndex = lastLdapUrlIndex;
        String host = ldapServerUrls[currentLdapUrlIndex];
        
        // TODO Optimizar este bucle que se me ha liado
        do {
            retryCount++;
            try {
            	if (LOG.isDebugEnabled()) LOG.debug("Trying to authenticate against {}", host);
                Hashtable<Object, Object> env = new Hashtable<Object, Object>();
                env.put(Context.INITIAL_CONTEXT_FACTORY, CONTEXT_FACTORY_CLASS);
                env.put(Context.PROVIDER_URL, host);
                env.put(Context.SECURITY_PRINCIPAL, username + "@" + domainName);
                env.put(Context.SECURITY_CREDENTIALS, password);
                DirContext ctx = new InitialDirContext(env);
                ctx.close();
                lastLdapUrlIndex = currentLdapUrlIndex;
                return true;
            } catch (CommunicationException exp) {
            	LOG.warn("Error al tratar de contactar con {}", host);
            	LOG.warn(exp.getMessage());
            	LOG.warn(exp.getExplanation());
                if (retryCount < ldapServerUrls.length) {
                    currentLdapUrlIndex++;
                    if (currentLdapUrlIndex == ldapServerUrls.length) {
                        currentLdapUrlIndex = 0;
                    }
                    continue;
                }
                return false;
            } catch (Throwable throwable) {
            	LOG.warn("Error al contactar con {}", host);
            	LOG.warn(throwable.getMessage());
                return false;
            }
        } while (true);
    }
    
    
    /**
     * @return
     */
    public String getDomain() {
    	return domainName;
    }
    
    
    /**
     * @param argDomain
     * @return
     * @throws Exception
     */
    private static String[] nsLookup(String argDomain) throws Exception {
        try {
            Hashtable<Object, Object> env = new Hashtable<Object, Object>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
            env.put("java.naming.provider.url", "dns:");
            DirContext ctx = new InitialDirContext(env);
            Attributes attributes = ctx.getAttributes(String.format("_ldap._tcp.%s", argDomain), new String[] { "srv" });
            // try thrice to get the KDC servers before throwing error
            for (int i = 0; i < 3; i++) {
                Attribute a = attributes.get("srv");
                if (a != null) {
                    List<String> domainServers = new ArrayList<String>();
                    NamingEnumeration<?> enumeration = a.getAll();
                    while (enumeration.hasMoreElements()) {
                        String srvAttr = (String) enumeration.next();
                        // the value are in space separated 0) priority 1)
                        // weight 2) port 3) server
                        String values[] = srvAttr.toString().split(" ");
                        domainServers.add(String.format("ldap://%s:%s", values[3], values[2]));
                    }
                    String domainServersArray[] = new String[domainServers.size()];
                    domainServers.toArray(domainServersArray);
                    return domainServersArray;
                }
            }
            throw new Exception("Unable to find srv attribute for the domain " + argDomain);
        } catch (NamingException exp) {
            throw new Exception("Error while performing nslookup. Root Cause: " + exp.getMessage(), exp);
        }
    }
    
    
    /**
     * 
     * @return
     */
    private String getLocalDomain() {
    	String domainName = null;
    	InetAddress ipAddress;
		try {
			ipAddress = InetAddress.getLocalHost();
			String cHostName = ipAddress.getCanonicalHostName();
			if (cHostName == null) {
				return null;
			}
			int idx = cHostName.indexOf('.');
	    	if (idx > -1) {
	    		domainName = cHostName.substring(idx + 1);
	    	}	    	
		} catch (UnknownHostException e) {
		}  
    	return domainName;
    }
    
}