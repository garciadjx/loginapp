/**
 * 
 */
package es.qworks.login.service.impl;

import javax.security.auth.login.LoginException;

import es.qworks.login.service.AuthenticationService;

/**
 * @author jmgarcia
 *
 */
@DBAuth
public class SimpleAuthenticationService implements AuthenticationService {

	private static final long serialVersionUID = 759634097190954173L;

	/**
	 * 
	 */
	public SimpleAuthenticationService() {
		
	}

	/**
	 * @see es.qworks.login.service.AuthenticationService#init()
	 */
	@Override
	public void init() throws Exception {
	}

	/**
	 * @see es.qworks.login.service.AuthenticationService#authenticate(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean authenticate(String username, String password) throws LoginException {
		
		return true;
	}

}
