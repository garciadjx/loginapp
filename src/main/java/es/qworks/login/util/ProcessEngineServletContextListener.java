package es.qworks.login.util;

import java.util.Set;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2014
 *
 */
@WebListener
public class ProcessEngineServletContextListener implements
		ServletContextListener {

	private static Logger LOG = LoggerFactory
			.getLogger(ProcessEngineServletContextListener.class);



	/**
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		LOG.info("Destroying process engines");
	}

	/**
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		LOG.info("Initializing process engines");

		listAllBeans();
	}

	//
	//
	//
	

	
	private void listAllBeans() {

        InitialContext initialContext;
		try {
			initialContext = new InitialContext();
			BeanManager bm = (BeanManager) initialContext.lookup("java:comp/BeanManager");
			if (bm == null) {
	        	System.err.println("No BeanManager available!!");
	        } else {
	            // List all CDI Managed Beans and their EL-accessible name
	            @SuppressWarnings("serial")
				Set<Bean<?>> beans = bm.getBeans(Object.class,new AnnotationLiteral<Any>() {});
	            for (Bean<?> bean : beans) {
	                System.out.println(bean.getBeanClass().getName() + " [" + bean.getName() + "]");
	            }
	        }

		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

}
