/*
 * 
 */
package es.qworks.login.dao;

import java.io.Serializable;

import es.qworks.login.domain.SimpleUser;

/**
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2015
 *
 */
public interface BasicUserDao extends Serializable {

	/**
	 * @param id
	 * @return
	 */
	public abstract SimpleUser getUserById(Integer id);
			
	/**
	 * @param login
	 * @return
	 */
	public abstract SimpleUser getUserByLogin(String login);
	
	/**
	 * @param login
	 * @return
	 */
	public abstract SimpleUser getUserByLogin(String login, String password);

}
