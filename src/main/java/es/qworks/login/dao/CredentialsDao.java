/*
 * Version 1.0 4/12/2015
 * 
 *
 * 2013 � Jos� M. Garc�a Dom�nguez
 *
 */
package es.qworks.login.dao;

import es.qworks.login.domain.SimpleUser;

/**
 * @author jmgarcia
 * @version 1.0
 */
public interface CredentialsDao {

	/**
	 * @param login
	 * @param password
	 * @return
	 */
	public SimpleUser getCredentials(String login, String password);
	
}
