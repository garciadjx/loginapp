/*
 * 
 */
package es.qworks.login.beans;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.qworks.login.service.LoginService;

/**
 * 
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2014
 */
@Named
@RequestScoped
public class LoginBean implements Serializable {
	
	private static final long serialVersionUID = -591211404614836926L;
	private static Logger LOG = LoggerFactory.getLogger(LoginBean.class);

	public static final String LOGIN_OK		= "index";
	public static final String LOGIN_ERROR	= "login";
	public static final String LOGOUT		= "logout";
	
	@Inject
	private LoginService loginService;
	
	private String name;
	private String password;
	
	/**
	 * Constructor 
	 */
	public LoginBean() {
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return
	 */
	public String doLogin() {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("Intentando login del usuario {}", name);
		}
		
		if (loginService.authenticate(name, password)) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Login satisfactorio del usuario {}", name);
			}
			return LOGIN_OK;
		}
		else {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Login fallido del usuario {}", name);
			}
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Error", "Invalid credentials"));
			return LOGIN_ERROR;
		}
	}
	
	/**
	 * @return
	 */
	public String doLogout() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Logout del usuario {}", loginService.getCurrentUser().getId());
		}	
		loginService.setCurrentUser(null);
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return LOGOUT;
	}
	
}
