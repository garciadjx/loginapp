package es.qworks.login.domain;

import java.io.Serializable;

public interface User<K extends Serializable> extends Serializable {

	/**
	 * @return the id
	 */
	public abstract K getId();

	/**
	 * @param id the id to set
	 */
	public abstract void setId(K id);

	/**
	 * @return the login
	 */
	public abstract String getLogin();

	/**
	 * @param login the login to set
	 */
	public abstract void setLogin(String login);

	/**
	 * @return the password
	 */
	public abstract String getPassword();

	/**
	 * @param password the password to set
	 */
	public abstract void setPassword(String password);

}