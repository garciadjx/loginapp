/*
 * Version 1.0 4/12/2015
 * 
 *
 * 2013 � Jos� M. Garc�a Dom�nguez
 *
 */
package es.qworks.login.domain;

/**
 * @author jmgarcia
 * @version 1.0
 */
public class Credentials {

	private Integer id;
	private String login;
	private String password;
	
	/**
	 * Constructor
	 */
	public Credentials() {
	}

	
	/**
	 * Constructor
	 * @param id
	 * @param login
	 * @param password
	 */
	public Credentials(Integer id, String login, String password) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
	}


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
