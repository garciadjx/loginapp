package es.qworks.login.domain;

import javax.inject.Named;

/**
 * @author J.M.Garc�a (jmgarcia@povisa.es) � 2015
 */
@Named
@CurrentUser
public class SimpleUser implements User<Integer> {

	private static final long serialVersionUID = 7155289881316654317L;
	
	private Integer id;
	private String login;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	
	/**
	 * Constructor 
	 */
	public SimpleUser() {
		
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @see es.qworks.login.domain.User#getLogin()
	 */
	@Override
	public String getLogin() {
		return login;
	}

	/**
	 * @see es.qworks.login.domain.User#setLogin(java.lang.String)
	 */
	@Override
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @see es.qworks.login.domain.User#getPassword()
	 */
	@Override
	public String getPassword() {
		return password;
	}

	/**
	 * @see es.qworks.login.domain.User#setPassword(java.lang.String)
	 */
	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
